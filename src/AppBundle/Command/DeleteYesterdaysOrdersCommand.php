<?php
namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class DeleteYesterdaysOrdersCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:delete-yesterdays-orders')
            ->setDescription('Delete yesterdays orders')
            ->setHelp('This command allows you to Delete yesterdays orders...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $io = new SymfonyStyle($input, $output);
        $totalCount = $em
            ->getRepository(\AppBundle\Entity\TestOrder::class)
            ->getYesterdaysOrdersCount();
        $batchSize = 10000;
        $steps = ceil($totalCount / $batchSize);
        $step = 0;

        $output->writeln([
            'Deleting yesterdays orders',
            '==========================',
            '',
        ]);
        $io->progressStart($steps);

        while ($step <= $steps) {
            $this->getContainer()->get('doctrine')
                 ->getRepository(\AppBundle\Entity\TestOrder::class)
                 ->deleteYesterdaysOrder($batchSize);

            if ($step == $steps) {
                // todo: reset OrdersCount for this date here if need
                break;
            }

            $io->progressAdvance();
            $step++;
            usleep(50000);
        }
    }
}