<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TestOrder
 *
 * @ORM\Table(name="test_order")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TestOrderRepository")
 */
class TestOrder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_facebook_id", type="integer")
     */
    private $userFacebookId;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float")
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=4)
     */
    private $currency;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_legal_entity", type="boolean")
     */
    private $isLegalEntity;

    /**
     * @var string
     *
     * @ORM\Column(name="params", type="string")
     */
    private $params;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="order_date", type="date")
     */
    private $orderDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userFacebookId
     *
     * @param integer $userFacebookId
     *
     * @return TestOrder
     */
    public function setUserFacebookId($userFacebookId)
    {
        $this->userFacebookId = $userFacebookId;

        return $this;
    }

    /**
     * Get userFacebookId
     *
     * @return int
     */
    public function getUserFacebookId()
    {
        return $this->userFacebookId;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return TestOrder
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return TestOrder
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set isLegalEntity
     *
     * @param boolean $isLegalEntity
     *
     * @return TestOrder
     */
    public function setIsLegalEntity($isLegalEntity)
    {
        $this->isLegalEntity = $isLegalEntity;

        return $this;
    }

    /**
     * Get isLegalEntity
     *
     * @return bool
     */
    public function getIsLegalEntity()
    {
        return $this->isLegalEntity;
    }

    /**
     * Set params
     *
     * @param string $params
     *
     * @return TestOrder
     */
    public function setParams($params)
    {
        $this->params = $params;

        return $this;
    }

    /**
     * Get params
     *
     * @return string
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Set orderDate
     *
     * @param \DateTime $orderDate
     *
     * @return TestOrder
     */
    public function setOrderDate($orderDate)
    {
        $this->orderDate = $orderDate;

        return $this;
    }

    /**
     * Get orderDate
     *
     * @return \DateTime
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return TestOrder
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}

