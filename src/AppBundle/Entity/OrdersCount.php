<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrdersCount
 *
 * @ORM\Table(name="orders_count")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrdersCountRepository")
 */
class OrdersCount
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", unique=true)
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\Column(name="count_orders", type="integer")
     */
    private $countOrders;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return OrdersCount
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set countOrders
     *
     * @param integer $countOrders
     *
     * @return OrdersCount
     */
    public function setCountOrders($countOrders)
    {
        $this->countOrders = $countOrders;

        return $this;
    }

    /**
     * Get countOrders
     *
     * @return int
     */
    public function getCountOrders()
    {
        return $this->countOrders;
    }
}

