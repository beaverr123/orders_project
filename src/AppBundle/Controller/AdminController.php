<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\{Request, JsonResponse};

class AdminController extends Controller
{
    /**
     * @Route("/admin/index", name="adminindex")
     */
    public function indexAction(Request $request)
    {
        //return new JsonResponse(array('result' => print_r(1,1)));

        return $this->render('admin/index.html.twig');
    }

    /**
     * @Route("/admin/yesterdaysorders/{page}", name="yesterdaysorders", requirements={"page"="\d+"})
     */
    public function yesterdaysOrdersAction(Request $request, $page = 1)
    {
        $result = $this->getDoctrine()
            ->getRepository(\AppBundle\Entity\TestOrder::class)
            ->getYesterdaysOrdersPaged($page);

        return new JsonResponse(array('result' => json_encode($result)));
    }

    /**
     * @Route("/admin/getMaxPageNum", name="getMaxPageNum")
     */
    public function getMaxPageNumAction(Request $request)
    {
        $result = $this->getDoctrine()
            ->getRepository(\AppBundle\Entity\TestOrder::class)
            ->getMaxPageNum();

        return new JsonResponse(array('result' => $result));
    }
}
