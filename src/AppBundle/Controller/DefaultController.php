<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\{Request, JsonResponse};

use AppBundle\Entity\TestOrder;

class DefaultController extends Controller
{


    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/createorder", name="createorder")
     */
    public function createOrderAction(Request $request)
    {
        $requestBody = json_decode($request->getContent()); // todo: why $request->request->get('facebook_id') is empty??
        $result = $this->getDoctrine()
            ->getRepository(TestOrder::class)
            ->createOrder($requestBody->facebook_id,$requestBody->amount,$requestBody->currency,$requestBody->is_legal_entity,$requestBody->params);

        return new JsonResponse(array('result' => $result));
    }

    /**
     * @Route("/filltable", name="filltable")
     */
    // only for test data generating
/*    public function fillTableAction(Request $request)
    {
        $result = $this->getDoctrine()
            ->getRepository(TestOrder::class)
            ->fillTable();

        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
       // return new JsonResponse(array('result' => print_r($result,1)));
    }*/
}
