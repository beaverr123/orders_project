angular.module('myApp', [])
    .controller('indexController', function($scope, $http) {
        $scope.facebook_id = null;
        $scope.currency = null;
        $scope.amount = null;
        $scope.is_legal_entity = false;
        $scope.params = [];

        $scope.createOrder = function() {

            $http({
                method: 'POST',
                url: 'createorder',
                data: {
                    facebook_id: $scope.facebook_id,
                    currency: $scope.currency,
                    amount: $scope.amount,
                    is_legal_entity: $scope.is_legal_entity,
                    params: angular.toJson($scope.params)

                }
            }).then((response) => {
                if(response.data.result == true){
                    alert('Order created!');
                }else{
                    alert('Something went wrong. Try later.');
                }
            }).catch((err) => {
                console.log('error');
            });
        };

        $scope.addParamField = function() {
            $scope.params.push({'key':null,'value':null});
        }

        $scope.removeParamField = function() {
            $scope.params.splice(-1,1);
        }
    })
    .controller('adminController', function($scope, $http) {
        $scope.page = null;
        $scope.toPage = null;
        $scope.maxPageNum = null;
        $scope.orders = [];
        $scope.isLoading = false;

        $scope.loadOrders = function(page){
            $scope.orders = [];
            $scope.isLoading = true;

            $http({
                method: 'GET',
                url: '/admin/yesterdaysorders/'+page,
            }).then((response) => {
                $scope.isLoading = false;
                $scope.page = page;
                $scope.orders = angular.fromJson(response.data.result);
            }).catch((err) => {
                console.log('error loadOrders');
            });
        };

        $scope.getMaxPageNum = function() {
            $http({
                method: 'GET',
                url: '/admin/getMaxPageNum',
            }).then((response) => {
                $scope.maxPageNum = response.data.result;
            }).catch((err) => {
                console.log('error');
            });
        };

        $scope.gotoPage = function(){
            if($scope.toPage > $scope.maxPageNum){
                $scope.toPage = $scope.maxPageNum
            }

            $scope.loadOrders($scope.toPage);
        }

        $scope.init = function () {
            $scope.getMaxPageNum();
            $scope.loadOrders(1);
        };
    });